"""
Helper functions for converting integers to strings.
"""


def int2str(i, chars):
    """
    Converts an integer to a string representation with multiple characters.

    Args:
        i: Integer to be converted to string.
        chars: The available characters of the representation.

    Returns:
        String representation of integer i.
    """
    s = ""
    while i:
        s += chars[i % len(chars)]
        i //= len(chars)
    return s[::-1]
