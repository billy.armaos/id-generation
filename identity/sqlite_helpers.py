"""
Helper functions for interacting with the sqlite database.
"""

import sqlite3


def create_connection():
    """
    Creates a connection to the sqlite database.

    Returns:
        Sqlite3 connection object.
    """
    return sqlite3.connect('db.sqlite3')


def create_database_and_table():
    """
    Creates an sqlite database and a table to store the data.

    Returns:
        String representation of integer i.
    """
    with create_connection() as conn:
        conn.execute('CREATE TABLE IF NOT EXISTS integer_ids (id integer PRIMARY KEY);')
