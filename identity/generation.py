# -*- coding: utf-8 -*-
"""
Functions for generating unique alphanumeric ids
"""

import sqlite3
import threading

from identity.constants import ID_CHARACTERS
from identity.sqlite_helpers import create_database_and_table, create_connection
from identity.string import int2str

lock = threading.Lock()


def _generate_bulk(number_of_ids):
    """
    Generates a list unique ids.

    Args:
        number_of_ids: Number of unique ids to be generated.

    Returns:
        List of unique alphanumeric ids.
    """
    with lock:
        with create_connection() as conn:
            conn.execute('BEGIN')
            old_max_cur = conn.execute('SELECT max(id) from integer_ids')
            conn.executemany('INSERT INTO integer_ids DEFAULT VALUES', [() for _ in range(number_of_ids)])

    old_max = old_max_cur.fetchone()[0] or 0
    alphanumeric_ids = [int2str(integer_id, ID_CHARACTERS).zfill(7)
                        for integer_id in range(old_max, old_max + number_of_ids)]
    return alphanumeric_ids


def generate_bulk(number_of_ids):
    """
    Generates a list unique ids. If the database does not exist it is created.

    Args:
        number_of_ids: Number of unique ids to be generated.

    Returns:
        List of unique alphanumeric ids.
    """
    try:
        return _generate_bulk(number_of_ids)
    except sqlite3.OperationalError:
        create_database_and_table()
        return _generate_bulk(number_of_ids)


def generate():
    """
    Generates a unique id.

    Returns:
        Unique alphanumeric id.
    """
    return generate_bulk(1)[0]
