# Notes on the ID Generation System

## About the implementation

This implementation of the ID generation system uses an SQLite database to achieve persistence.
A single table is used that contains only a single column that is an integer primary key.
Entries are added to the database, which automatically increments the primary key.
The integer values produced are then mapped to alphanumeric ids which are returned to the user.

### Benefits

The main benefit of using a database for such a task is that it guarantees persistence.

This implementation is rather fast for some types of workloads.
Since ids are created in an incremental manner, no checks need to be performed to test whether a new ID has been
generated before or not.

### Drawbacks

The implementation as it is developed performs poorly in multithreading applications (although it does pass the
performance tests).
This is the result of the serial nature of primary keys.

To solve this problem, one could attempt to have each thread write integers starting from a few fixed but different
starting points.
This would help to avoid constant rollbacks (which are inevitable for the current implementation).
This would create the additional complexity of having to keep track of multiple integer ranges.
However, it would be a truly concurrent solution instead of an implementation that uses a simple lock.

In such an implementation, SQLAlchemy + PostgreSQL would be ideally used, since concurrent writers is not the strong
point of SQLite. However, this would increase the amount of external dependencies.



